@extends('layouts.master')

@section('source')
<link rel="stylesheet" href="css/blog/main.css">
@endsection

@section('content')
<div class="jumbotron">
    <div class="container">
        <h2>Classic example to show the usage of CRUD</h2>
        <h3>Create Read Update Destroy</h3>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-sm-3 col-xs-12 sidebar_area">
            <div class="panel panel-default sidebar">
                <a href="{{ route('blog.create') }}" class="btn btn-primary form-control">Create new Blog</a>
            </div>
        </div>        

        <div class="col-sm-9 col-xs-10 col-sm-offset-0 col-xs-offset-1">
            @foreach ($blogs as $blog)
                <div class="row panel panel-default blogpanel">
                    <div class="col-md-4 col-sm-8 col-xs-12">
                        <h3>{{ $blog->title }}</h3>
                        <h5>{{ $blog->subtitle }}</h5>
                        <p>{!! nl2br(substr($blog->content,0,50)) !!}...</p>
                        <a class="btn btn-primary" href="{{ route('blog.show',[$blog->id]) }}">read more</a>
                        <br><br>
                    </div>
                    <div class="col-md-4 col-md-hidden col-xs-hidden">
                        @if ($blog->blogimg)
                        <img src="{{ URL::asset('upload/'.$blog->blogimg) }}" alt="" class="blogimg hidden-xs" >
                        @endif
                    </div>
                    <div class="col-sm-4 col-xs-12">
                        <div class="created_dt">{{ substr($blog->created_at,0,10) }}</div>
                        <a href="{{ route('blog.edit', $blog->id) }}" class="btn btn-success form-control margin-d5">Edit</a>
                        <button onclick="blog_destroy({{ $blog->id }})" class="btn btn-warning form-control" id="delblog">Destroy</button>
                    </div>
                </div>
            @endforeach
            
        </div>
    </div>
</div>
    
@endsection

@section('script')
<script>
function blog_destroy(id){
    if(confirm("Are you sure do delete this blog?")){
        $.ajax({
            url: "{{ url('/blog') }}/"+id,
            method: "DELETE",
            data: {
                _token: '{{ csrf_token() }}'
            },
            success: function(){
                location.reload();
            }
        });
    } else {
        return;
    }
}
</script>
@endsection