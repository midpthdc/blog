@extends('layouts.master')

@section('source')
<link rel="stylesheet" href="{{ URL::asset('css/blog/show.css') }}">
@endsection

@section('content')
    <div class="jumbotron">
        <div class="container">
            <h1>This is Edit blade</h1>
            <h3>Which updates specific item</h3>
            <a href="{{ route('blog.index') }}" class="btn btn-primary"><i class="glyphicon glyphicon-arrow-left"></i> Back</a>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 panel panel-default blog-area">
                <form action="{{ route('blog.update',$blog->id) }}" method="POST" enctype="multipart/form-data">
                    <div class="form-group">
                        {{ csrf_field() }}
                        {{ method_field('PATCH') }}
                        <label for="title">Title: </label>
                        <input type="text" name="title" class="form-control" value="{{ $blog->title }}">
                        <label for="subtitle">Subtitle: </label>
                        <input type="text" name="subtitle" class="form-control" value="{{ $blog->subtitle }}">
                        <label for="blogimg">Image</label>
                        @if ($blog->blogimg)
                        <input id="edit-file" type="file" name="blogimg" value="{{ URL::asset('upload/'.$blog->blogimg) }}">
                        {{-- <img src="{{ URL::asset('upload/'.$blog->blogimg) }}" alt="" id="edit-img"> --}}
                        @else
                        <input id="edit-file" type="file" name="blogimg" value="">
                        {{-- <img src="" alt="" id="edit-img"> --}}
                        @endif
                        <label for="content">Content: </label>
                        <textarea id="my-textarea" name="content" cols="30" rows="10" class="form-control" >{{ $blog->content }}</textarea>
                        <input type="submit" value="Update" class="btn btn-success form-control">
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('script')
<script>
enableTab('my-textarea');
$("#edit-file").change(function(){
    $("#edit-img").attr('src',$("#edit-file").attr('value'));
});
</script>
@endsection