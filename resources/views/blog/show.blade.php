@extends('layouts.master')

@section('source')
<link rel="stylesheet" href="{{ URL::asset('css/blog/show.css') }}">
@endsection

@section('content')
    <div class="jumbotron">
        <div class="container">
            <h1>This is show blade</h1>
            <h3>Which shows specific item</h3>
            <a href="{{ route('blog.index') }}" class="btn btn-primary"><i class="glyphicon glyphicon-arrow-left"></i> Back</a>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 panel panel-default blog-area">
                <h1>{{ $blog->title }}</h1>
                <h3>{{ $blog->subtitle }}</h2>
                <img src="{{ URL::asset('upload/'.$blog->blogimg) }}" class="showimg" alt="">
                <pre>{{ $blog->content }}</pre>
                <a href="{{ route('blog.edit', $blog->id) }}" class="btn btn-success form-control margin-d5">Edit</a>
                <button onclick="blog_destroy({{ $blog->id }})" class="btn btn-warning form-control margin-d5" id="delblog">Destroy</button>
            </div>
        </div>
    </div>
@endsection

@section('script')
<script>
function blog_destroy(id){
    if(confirm("Are you sure do delete this blog?")){
        $.ajax({
            url: "{{ url('/blog') }}/"+id,
            method: "DELETE",
            data: {
                _token: '{{ csrf_token() }}'
            },
            success: function(){
                location.reload();
            }
        });
    } else {
        return;
    }
}
</script>
@endsection