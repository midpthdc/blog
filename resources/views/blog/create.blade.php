@extends('layouts.master')

@section('source')
<link rel="stylesheet" href="{{ URL::asset('css/blog/create.css') }}">
@endsection

@section('content')
    <div class="jumbotron">
        <div class="container">
            <h1>This is Create blade</h1>
            <h3>Which creates a new item</h3>
            <a href="{{ route('blog.index') }}" class="btn btn-primary"><i class="glyphicon glyphicon-arrow-left"></i> Back</a>
        </div>
    </div>
    <div class="container">
        <form action="{{ route('blog.store') }}" method="POST" class="create_form" enctype="multipart/form-data">
            <div class="form-group">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <label for="title">Title: </label>
                <input type="text" name="title" class="form-control">
                <label for="subtitle">Subtitle: </label>
                <input type="text" name="subtitle" class="form-control">
                <label for="blogimg">Image</label>
                <input type="file" name="blogimg">
                <label for="content">Content: </label>
                <textarea id="my-textarea" name="content" name="content" cols="30" rows="10" class="form-control"></textarea>
                <input type="submit" value="Create" class="btn btn-primary form-control">
            </div>
        </form>
    </div>
@endsection
        

@section('script')
<script>
    enableTab('my-textarea');
</script>
@endsection