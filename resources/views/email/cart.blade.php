<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Strict//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd'>
<html xmlns='http://www.w3.org/1999/xhtml'>
<head>
<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
<style>
	body{
		font-family: Arial, Helvetica, sans-serif;
		font-size:12px;color:#333333;line-height:14px;
	}
</style>
</head>
<body>
<div style='padding:5px;width:100%;' align='center'>
	<table width='95%' border='0' align='center' cellpadding='0' cellspacing='0' style='border-collapse: collapse;'>
		<tr>
			<td >
				<div style='width:100%;font-size:14px;line-height:25px;text-align:right;'>
					訂購日期：<span style='text-decoration: underline;'>{{ $orderTime }}</span>&nbsp;
					訂單編號：<span style='text-decoration: underline;'>{{ $orders_id }}</span>
				</div>
			</td>
		</tr>
	</table>
	<table width='95%' border='0' align='center' cellpadding='0' cellspacing='0' style='border-collapse: collapse;'>
		<tr>
			<td >
				<div style='width:100%;font-size:18px;font-weight:bold;line-height:30px;letter-spacing:5px;text-align:center;background:#d7d8db;'>
					&nbsp;&nbsp;XX統合行銷網 訂購單&nbsp;&nbsp;
				</div>
			</td>
		</tr>
	</table><br>
	<table width='95%' border='0' align='center' cellpadding='0' cellspacing='0' style='border-collapse: collapse;' bgcolor='#d7d8db'>
		<tr bgcolor='#d7d8db'>
			<td align='left' width='100%' height='20' colspan='2'  style='border:1px solid #d7d8db;padding:3px;font-size: 12px;color:#333333;font-weight:bold;'>■ 訂購資料</td>
		</tr>
		<tr>
			<td width='13%' height='25' align='center' bgcolor='#E7E7E7' style='border:1px solid #d7d8db;padding:3px;font-size: 12px;'>中文姓名</td>
			<td align='left' width='87%' bgcolor='#FFFFFF'  style='border:1px solid #d7d8db;padding:3px;font-size: 12px;color:#333333;'>{{ $order_name }}</td>
		</tr>
		<tr>
			<td width='13%' height='25' bgcolor='#E7E7E7' align='center' style='border:1px solid #d7d8db;padding:3px;font-size: 12px;'>聯絡電話</td>
			<td align='left' width='87%' bgcolor='#FFFFFF'  style='border:1px solid #d7d8db;padding:3px;font-size: 12px;color:#333333;'>{{ $order_tel }}</td>
		</tr>
		<tr>
			<td width='13%' height='25' bgcolor='#E7E7E7' align='center' style='border:1px solid #d7d8db;padding:3px;font-size: 12px;'>電子郵件</td>
			<td align='left' width='87%' bgcolor='#FFFFFF'  style='border:1px solid #d7d8db;padding:3px;font-size: 12px;color:#333333;'>{{ $order_email }}</td>
		</tr>
		<tr>
			<td width='13%' height='25' bgcolor='#E7E7E7' align='center' style='border:1px solid #d7d8db;padding:3px;font-size: 12px;'>收件地址</td>
			<td align='left' width='87%' bgcolor='#FFFFFF'  style='border:1px solid #d7d8db;padding:3px;font-size: 12px;color:#333333;'>{{ $order_address }}</td>
		</tr>
		<tr>
			<td height='25' align='center' bgcolor='#E7E7E7' style='border:1px solid #d7d8db;padding:3px;font-size: 12px;'>收件時間</td>
			<td align='left' bgcolor='#FFFFFF' style='border:1px solid #d7d8db;padding:3px;font-size: 12px;color:#333333;'>{{ $sendTime }}</td>
		</tr>
		<tr>
			<td height='25' align='center' bgcolor='#E7E7E7' style='border:1px solid #d7d8db;padding:3px;font-size: 12px;'>備註事項</td>
			<td align='left' bgcolor='#FFFFFF' style='border:1px solid #d7d8db;padding:3px;font-size: 12px;color:#333333;'>{{ $order_memo }}</td>
		</tr>
		<tr>
			<td height='25' align='center' bgcolor='#E7E7E7' style='border:1px solid #d7d8db;padding:3px;font-size: 12px;'>付款方式</td>
			<td align='left' bgcolor='#FFFFFF' style='border:1px solid #d7d8db;padding:3px;font-size: 12px;color:#333333;'>{{ $payType }}</td>
		</tr>
		<tr>
			<td height='25' align='center' bgcolor='#E7E7E7' style='border:1px solid #d7d8db;padding:3px;font-size: 12px;'>付款狀態</td>
			<td align='left' bgcolor='#FFFFFF' style='border:1px solid #d7d8db;padding:3px;font-size: 12px;color:#333333;'>{{ $payStatus }}</td>
		</tr>
	</table><br>
	<table width='95%' border='0' cellspacing='0' cellpadding='0' style=' border-collapse: collapse;'  align='center'>
		<tr>
			<td >
				<table width='100%' border='0' cellpadding='0' cellspacing='0' style=' border-collapse: collapse;font-size:11px;'>
					<tr bgcolor='#E7E7E7' height='30'>
						<td width='10%' align='center' style='border:1px solid #d7d8db;padding:3px;font-size: 12px;color:#333333;'>編號</td>
						<td width='40%' align='center' style='border:1px solid #d7d8db;padding:3px;font-size: 12px;'>產品名稱</td>
						<td width='10%' align='center' style='border:1px solid #d7d8db;padding:3px;font-size: 12px;'>單價</td>
						<td width='10%' align='center' style='border:1px solid #d7d8db;padding:3px;font-size: 12px;'>數量</td>
						<td width='10%' align='center' style='border:1px solid #d7d8db;padding:3px;font-size: 12px;'>金額</td>
					</tr>
					{!! $Order_Items !!}
				</table>
				<div style='line-height:16px;padding:3px;padding-right:5px;'>
					<div style='text-align:right;font-size: 12px;color:#333333;'>合計&nbsp;${{ $Items_Total }}</div>
					<div style='text-align:right;font-size: 12px;color:#333333;'>運費：$ {{ $freight }}</div>
					<div style='text-align:right;font-size: 12px;color:#333333;'><b>總額&nbsp;</b> <b>${{ $orderPrice }}</b></div>
				</div>
			</td>
		</tr>
	</table>
</div>
</body>
</html>