{{-- email --}}
@extends('layouts.master')

@section('source')
@endsection

@section('content')
<div class="jumbotron">
    <div class="container">
        <h1>Email</h1>
        <h3>填寫您的信箱與名字，送出將寄出一封測試信到您填寫的信箱</h3>
    </div>
</div>
<div class="container">
    <div class="progress" style="display:none;" id="waitbar">
        <div class="progress-bar progress-bar-striped active" role="progressbar"
        aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width:80%;">
            寄出中...請稍候...
        </div>
    </div>
    <form class="create_form">
        <div class="form-group">
            <label for="name">Name: </label>
            <input type="text" id="fm_name" class="form-control">
            <label for="email">Email: </label>
            <input type="text" id="fm_email" class="form-control">
            <label for="comment">Comment: </label>
            <textarea id="my-textarea" cols="30" rows="10" class="form-control"></textarea>
        </div>
    </form>
    <div class="row" style="margin-bottom: 50px">
        <div class="col-md-1"></div>
        <div class="col-md-5">
            <button class="btn btn-primary form-control margin-d5" onclick="normal_send()">寄出(一般信件)</button>
        </div>
        <div class="col-md-5">
            <button class="btn btn-primary form-control margin-d5" onclick="cart_send()">寄出(模擬購物收據)</button>
        </div>
        <div class="col-md-1"></div>
    </div>
</div>
@endsection

@section('script')
<script>

function normal_send(){
    $("#waitbar").slideDown();
    // console.log($("#fm_name").val());return;
    $.ajax({
        url: "{{ url('email/normal') }}",
        method: "POST",
        data: {
            _token: "{{ csrf_token() }}",
            name: $("#fm_name").val(),
            email: $("#fm_email").val(),
            comment: $("#my-textarea").val(),
        },
        success: function(res){
            $("#waitbar").slideUp();
            if (res.ok=='t') {
                alert('信件已寄出!');location.reload();
            } else {
                alert('寄信出錯: '+ res.msg);
            }
        },
        error: function(xhr, ajaxOptions, thrownError){
            $("#waitbar").slideUp();
            alert("頁面發生錯誤!請聯絡系統管理員");
            console.log(xhr.status);
            console.log(thrownError);
        },
        dataType: "json"
    });    
}

function cart_send(){
    $("#waitbar").slideDown();
    $.ajax({
        url: "{{ url('email/cart') }}",
        method: "POST",
        data: {
            _token: "{{ csrf_token() }}",
            name: $("#fm_name").val(),
            email: $("#fm_email").val(),
            comment: $("#my-textarea").val(),
        },
        success: function(res){
            $("#waitbar").slideUp();
            if (res.ok=='t') {
                alert('信件已寄出!');location.reload();
            } else {
                alert('寄信出錯: '+ res.msg);
            }
        },
        error: function(xhr, ajaxOptions, thrownError){
            $("#waitbar").slideUp();
            alert("頁面發生錯誤!請聯絡系統管理員");
            console.log(xhr.status);
            console.log(thrownError);
        },
        dataType: "json"
    });
}
</script>
@endsection