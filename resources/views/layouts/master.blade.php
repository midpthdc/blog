<html>
<head>
	<meta charset="UTF-8">
	<title></title>
	<link rel="stylesheet" href="{{ URL::asset('bootstrap/css/bootstrap-theme.min.css') }}" type="text/css">
	<link rel="stylesheet" href="{{ URL::asset('bootstrap/css/bootstrap.min.css') }}" type="text/css">
	<link rel="stylesheet" href="{{ URL::asset('css/common.css') }}" type="text/css">
	
	<script src="{{ URL::asset('js/jquery.min.js')}}"></script>
	<script src="{{ URL::asset('bootstrap/js/bootstrap.min.js')}}"></script>
	<script src="{{ URL::asset('js/vue.js')}}"></script>
	<script src="{{ URL::asset('js/common.js') }}"></script>
	@section('source')
	@show
</head>

<body>
@include('layouts.nav')
@yield('content')
	
</body>
</html>
<script>
// $(window).scroll(function(evt){
	// if ($(window).scrollTop()>0)
	// 	$(".navbar").removeClass("navbar-top");
	// else
	// 	$(".navbar").addClass("navbar-top");
// });
</script>
@section('script')
@show