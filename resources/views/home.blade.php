@extends('layouts.master')

@section('head')
@endsection


@section('source')
<link rel="stylesheet" href="{{ URL::asset('css/style.css') }}" type="text/css">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">
<script src="{{ URL::asset('js/skrollr.js') }}"></script>
<script src="{{ URL::asset('js/scripts.js') }}"></script>
@endsection


@section('content')
<div class="jumbotron">
	<img src="{{ URL::asset('image/unnamed.jpg') }}" alt=""
		data-0="transform: translate(0px,0px)" 
		data-100="transform: translate(0px,-50%)">
</div>

<section class="sec_about">
<div class="container">
<div class="row">
	<div class="col-xs-5">
		<img class="headimg"
			src="{{ URL::asset('image/banner.jpg') }}" alt="">
	</div>
	<div class="col-xs-7">
	<div class="row">
		<div class="col-xs-12">
			<h1>About Me</h1>
			<hr>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-12">
			<div class="icons">
				<i class="fas fa-graduation-cap"></i>
				<h3>學歷</h3>
			</div>
			<ul>
				<li>國立鳳山高級中學 普通科</li>
				<li>國立中興大學 資工系</li>
			</ul>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-12">
			<div class="icons">
				<i class="fab fa-php"></i>
				<h3>專長</h3>
			</div>
			<ul>
				<li>html/css</li>
				<li>php/laravel</li>
				<li>jquery/ajax/vue.js</li>
				<li>mysql</li>
				<li>C/python</li>
			</ul>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-12">
			<div class="icons">
				<i class="fas fa-book"></i>
				<h3>經歷</h3>
			</div>
			<ul>
				<li>家教 : 數學、化學、C語言</li>
				<li>山水雲林 : 後端助理工程師</li>
			</ul>
		</div>
	</div>
	</div>
</div>
</div>
</section>

<section class="sec_intro">
<div class="container">
<div class="row">
	<h1 class="col-xs-12">自傳</h1>
	
	<div class="col-xs-12 intros">
		<p>我來自高雄，去年2月開始在台中一間網頁公司實習，7月從國立台中中興大學資工系畢業，並持續實習的工作直到年底入伍當兵。於今年2018/4月退伍後給自己一個月的時間休息兼回溫框架的使用，預計5月份開始找新工作。</p>
		<br>
	<p>大學期間擅長的語言為 C 與 python，實習後開始熟悉與網站開發相關的語言(html/css/js/php/ C#/mysql...等)</p>
	<br>
	<p>實習期間協助公司幫許多不同框架的網站增設過各種功能，像是會員系統、編輯後台、寄信、瀏覽人數、輸出報表、輸出圖片、塗鴉板、排班表等功能。改過以Laravel、CI、smarty為框架的網站、asp.net網站等。有基本MVC觀念，近期開發的專案都以Laravel框架為主。</p>
	<br>
	<p>接觸網頁後端工程師工作的時間雖然不算長，開發速度還有待磨練，也還有很多技術等著我去學習，但我獨立性強，實習期間也都是自己邊學邊做，我很有興趣從事網頁開發，喜歡從工作中獲得成就感的喜悅，希望哪一天也能成為獨當一面的全端工程師。</p>
</div>
</div>
</div>
</section>

<section class="sec_exp">
<div class="container">
<div class="row">
	<h1 class="col-xs-12">開發經驗</h1>
	<div class="col-md-4 col-sm-6 col-xs-12 exp_item">
		<div class="row">
			<a href="http://www.finn-th.com/" target="_blank"><img class="col-xs-5" src="http://www.mid.com.tw/images/work02-16.jpg" alt=""></a>
			<div class="col-xs-7">
				<h4>finn</h4>
				<h6>CI框架購物網站</h6>
				<div class="comment">
					解決無法購買中／泰文商品的bug
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-12 exp_item">
		<div class="row">
			<a href="http://thtech.thu.edu.tw/?lnk=order_list" target="_blank"><img class="col-xs-5" src="https://external.fkhh1-1.fna.fbcdn.net/safe_image.php?d=AQCQphKkIkEvxAyA&w=540&h=282&url=http%3A%2F%2Fthtech.thu.edu.tw%2Fattach%2Fad%2F20180305135103_banner.jpg&cfs=1&upscale=1&fallback=news_d_placeholder_publisher&_nc_hash=AQDY67cbpY1HiucT" alt=""></a>
			<div class="col-xs-7">
				<h4>東海共儀</h4>
				<h6>學校預約器材頁面</h6>
				<div class="comment">
					多一個序號預約的列表
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-12 exp_item">
		<div class="row">
			<a href="http://touchdog.com.tw/index.php?op=edm" target="_blank"><img class="col-xs-5" src="https://external.fkhh1-1.fna.fbcdn.net/safe_image.php?d=AQDdd9nUOLH6uMWb&w=540&h=282&url=http%3A%2F%2Ftouchdog.com.tw%2Fupload%2Fbigphoto%2Fbanner20160603011816-1.jpg&cfs=1&upscale=1&fallback=news_d_placeholder_publisher&_nc_hash=AQCMUct3wGq-2xNO" alt=""></a>
			<div class="col-xs-7">
				<h4>承大寵物</h4>
				<h6>smarty網站</h6>
				<div class="comment">
					整個異業合作的後台管理(新增產業、商品、圖片、商品內容修改...等)<br>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-12 exp_item">
		<div class="row">
			<div class="col-xs-12">
				<a href="http://chenghu.com.tw/" target="_blank">
					<h4>晨竹統合行銷網</h4>
				</a>
				<h6>購物網站</h6>
				<div class="comment">
					一開始負責會員系統，<br>
					後來陸續完成包括購物車、寄信等...幾乎整個網站的後端功能<br>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-12 exp_item">
		<div class="row">
			<a href="http://新竹家電維修.tw" target="_blank"><img class="col-xs-5" src="http://www.mid.com.tw/images/work02-23.jpg" alt=""></a>
			<div class="col-xs-7">
				<h4>大新竹家電</h4>
				<h6>形象網站</h6>
				<div class="comment">
					負責將客戶送出的聯絡顯示到後台
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-12 exp_item">
		<div class="row">
			<div class="col-xs-12">
				<h4>威仕鵬</h4>
					<h6>內網系統</h6>
					<div class="comment">
						功能包含會員、紀錄貨品環境狀況、輸出報表等...<br>
					</div>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-12 exp_item">
		<div class="row">
			<div class="col-xs-12">
				<a href="http://web.cyshb.gov.tw/cmhc/index2.aspx" target="_blank"><h3>嘉義心理衛生中心</h6></a>
				<h6>.net網頁</h6>
				<div class="comment">
					瀏覽人次、寄信功能、固定弱掃維護備分工作
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-12 exp_item">
		<div class="row">
			<div class="col-xs-12">
				<h4>樂衍-醫院排班表</h4>
				<h6>內網系統</h6>
				<div class="comment">
					可月份複製、周複製的<br>
					醫生、櫃台、助理人員排班表<br>
				</div>
			</div>
		</div>
	</div>
	<div class="col-xs-6 exp_item">
		<div class="row">
			<div class="col-xs-12">
				<a href="http://www.leyan.tw/13" target="_blank"><h4>樂衍-牙醫系統</h4></a>
				<h6>相當相當龐大的案子</h6>
				<div class="comment">
					Laravel為框架，實作包含醫院掛號、病歷、預約、抽審、病患資料...等功能<br>
					其中串寫健保卡讀卡機API(掛號、補退卡、寫卡、IC認證、IC上傳....)<br>
					是由我負責，網站的開發也參與了不少，去年七月份到十二月都在忙這個案子<br>
					一直到現在該系統還持續在擴展更多功能
				</div>
			</div>
		</div>
	</div>



	

</div>
</div>
</section>
@endsection

@section('script')

<script>
	// $(window).on('load',function(){
	// 	var s = skrollr.init();

	// });
</script>
@endsection