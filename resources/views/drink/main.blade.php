{{-- drink.main --}}
@extends('layouts.master')

@section('source')
<link rel="stylesheet" href="{{ URL::asset('css/drink/main.css') }}">
@endsection

@section('content')

<div id="app">
  <div class="jumbotron" v-if="!hide_jumbo">
    <h1> Vue.js 練習 </h1>
  </div>
  <section class="menubar container">
    <div class="menu">
      <h3>Menu</h3>
      <h5 v-if="drinks.length==0">載入中...</h5>
      <table class="table table-striped">
        <thead>
          <tr>
            <th style="width: 20px;"></th>
            <th style="width: 30%;">品項</th>
            <th style="text-align: center;">M</th>
            <th style="text-align: center;">L</th>
            <th style="width: 4em;text-align: center;">移除</th>
          </tr>
        </thead>
        <tr v-for="(id,drink) in drinks">
          <td> <span class="glyphicon glyphicon-star" v-if="drink.is_hot"></span></td>
          <td>@{{drink.name}}</td>
          <td class="ordertd" v-on:click="order(id,'M',drink.mprice)">@{{drink.mprice<=0? '-':drink.mprice}}</td>
          <td class="ordertd" v-on:click="order(id,'L',drink.lprice)">@{{drink.lprice<=0? '-':drink.lprice}}</td>
          <td class="deltd" v-on:click="rm_drink(id)">X</td>
        </tr>
      </table>
    </div>
    <div class="btn btn-primary form-control" v-if="!show_addbar" v-on:click="display_addbar(true)">新增茶品</div>
    <div class="addbar" v-if="show_addbar">
      <div class="row">
        <div class="col-md-4 col-xs-12">
          <label class="input-group"><span class="input-group-addon">
              <input type="checkbox" v-model="new_drink.is_hot" value="true"/><span class="glyphicon glyphicon-star"></span><span class="label-text">熱門</span></span>
            <input class="form-control" placeholder="品項名稱" v-model="new_drink.name"/>
          </label>
        </div>
        <div class="col-md-4 col-sm-6 col-xs-12">
          <label class="input-group"><span class="input-group-addon">價位(M)</span>
            <input class="form-control" v-model="new_drink.mprice" type="number" name="mprice"/>
          </label>
        </div>
        <div class="col-md-4 col-sm-6 col-xs-12">
          <label class="input-group"><span class="input-group-addon">價位(L)</span>
            <input class="form-control" v-model="new_drink.lprice" type="number" name="lprice"/>
          </label>
        </div>
        <div class="col-xs-12">
          <div class="btn btn-success form-control" v-on:click="add_drink()">新增</div>
          <div class="btn btn-warning form-control" v-if="show_addbar" v-on:click="display_addbar(false)">隱藏新增列</div>
        </div>
      </div>
    </div>
  </section>
  <section class="orderlist container">
    <h3>結算清單</h3>
    <ul class="list-group">
      <li class="list-group-item active"><span class="list-col cate">項目</span><span class="list-col cate">L/M</span><span class="list-col cate">價錢</span><span class="list-col cate">糖度</span><span class="list-col cate">冰塊</span><span class="list-col cate">取消</span></li>
      <li class="list-group-item" v-for="(id,order) in orders"><span class="list-col">@{{order.name}}</span><span class="list-col">@{{order.size}}</span><span class="list-col">@{{order.price}}</span><span class="list-col"> 
          <select class="form-control sel_sugar">
            <option>正常</option>
            <option>半冰</option>
            <option>微冰</option>
            <option>去冰</option>
          </select></span><span class="list-col">
          <select class="form-control sel_ice">
            <option>正常</option>
            <option>半糖</option>
            <option>微糖</option>
            <option>無糖</option>
          </select></span><span class="list-col list-del" v-on:click="cancel(id)">X</span></li>
      <li class="list-group-item list-group-item-info"><span class="list-col">總價 : </span><span class="list-col">@{{total}}</span></li>
    </ul>
  </section>
</div>
@endsection

@section('script')
<script>
var vm = new Vue({
  el: "#app",
  data: {
    drinks: [],
    orders: [],
    show_addbar: false,
    new_drink: {
      name: '',
      mprice: 0,
      lprice: 0,
      is_hot: false
    }
  },
  methods: {
    display_addbar: function(show){
      console.log(show);
      this.show_addbar = show;
      // if(show) $(".addbar").slideDown();
      // else $(".addbar").slideUp();
    },
    add_drink: function(){
      var tmp = this.new_drink;
      if(this.new_drink.name=='') {
        alert('請輸入品項名稱!');return;
      }
      this.drinks.push({
        name: this.new_drink.name,
        mprice: this.new_drink.mprice,
        lprice: this.new_drink.lprice,
        is_hot: this.new_drink.is_hot
      });
    },
    rm_drink: function(id){
      this.drinks.splice(id,1);
    },
    order: function(id,size,price){
      console.log(price)
      if(price<=0) {
        // do nothing
      } else {
        this.orders.push({
          name: this.drinks[id].name,
          size: size,
          price: price
        });
      }
      console.log(this.orders);
    },
    cancel: function(id){
      this.orders.splice(id,1);
    }
  },
  computed: {
    total: function(){
      var total = 0;
      for( i = 0 ;i < this.orders.length; i++){
        total += this.orders[i].price;
      }
      return total;
    }
  },
  ready: function(){
    $.ajax({
      url: "drink/get_drinks",
      method: 'POST',
      data: {
        _token: '{{ csrf_token() }}'
      },
      dataType: 'json',
      success: function(res){
        vm.drinks = res;
      }
    });
  }
});
</script>
@endsection