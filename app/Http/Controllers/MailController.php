<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;
use Carbon\Carbon;

class MailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('email.main');
    }

    // 寄出一般信件
    public function normal(Request $request){
        $res = ['ok'=>'t','data'=>"",'msg'=>'信件已經出'];
        try {
            $this->validate($request,[
                'email'=>'required',
                'name'=>'required|max:20',
            ]);
            
            
            $from = [
                'email'=>"midpthdc@gmail.com",
                'name'=>"涵德(測試信件)",
                'subject'=>"這是一封測試信"
            ];
            //填寫收信人信箱
            $to = [
                'email'=>$request->email,
                'name'=>$request->name
            ];
            //信件的內容(即表單填寫的資料)
            $data = [
                'subject'=>$from['subject'],
                'msg'=>"你已經收到我的測試信~",
                'comment'=>$request->comment,
                'name' => $request->name,
            ];  
            //寄出信件
            Mail::send('email.normal', $data, function($message) use ($from, $to) {
                $message->from($from['email'], $from['name']);
                $message->to($to['email'], $to['name'])->subject($from['subject']);
            });
        } catch (Exception $e){
            $res = ['ok'=>$e->getMessage(),'data'=>$request,'msg'=>$e->getMessage()];
        }
        return response()->json($res);
    }

    // 寄出購物信件
    public function cart(Request $request){
        $res = ['ok'=>'t','data'=>"",'msg'=>'信件已經出'];
        try {
            $this->validate($request,[
                'email'=>'required',
                'name'=>'required|max:20',
            ]);

            $from = [
                'email'=>"midpthdc@gmail.com",
                'name'=>'涵德(測試信)',
                'subject'=>'這是一封測試信'
            ];

            $to = [
                'email'=>$request->email,
                'name'=>$request->name,
            ];

            $html = "<tr bgcolor='#E7E7E7' height='30'>
						<td width='10%' align='center' style='border:1px solid #d7d8db;padding:3px;font-size: 12px;color:#333333;'>001</td>
						<td width='40%' align='center' style='border:1px solid #d7d8db;padding:3px;font-size: 12px;'>涵德簽名照</td>
						<td width='10%' align='center' style='border:1px solid #d7d8db;padding:3px;font-size: 12px;'>1000</td>
						<td width='10%' align='center' style='border:1px solid #d7d8db;padding:3px;font-size: 12px;'>1</td>
						<td width='10%' align='center' style='border:1px solid #d7d8db;padding:3px;font-size: 12px;'>1000</td>
					</tr>";

            $data = [
                'orderTime' => Carbon::today()->format('Y-m-d'),
                'orders_id' => 'ABC123',
                'order_name' => $request->name,
                'order_tel' => '0987654321',
                'order_email' => $to['email'],
                'order_address' => '四海為家',
                'sendTime' => Carbon::tomorrow()->format('Y-m-d'),
                'order_memo' => $request->comment,
                'payType' => '超商付款',
                'payStatus' => '未付',
                'Items_Total' => 1000,
                'freight' => 0,
                'orderPrice' => 1000,
                'Order_Items' => $html,
            ];

            Mail::send('email.cart',$data,function($message) use ($from,$to){
                $message->from($from['email'],$from['name']);
                $message->to($to['email'],$to['name'])->subject($from['subject']);
            });

        } catch (Exception $e){
            $res = ['ok'=>$e->getMessage(),'data'=>$request,'msg'=>$e->getMessage()];
        }
        return response()->json($res);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
