<?php

namespace App\Http\Controllers;

use App\Blog;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $blogs = Blog::orderBy('id','desc')->get();
        return view('blog.main',['blogs'=>$blogs]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('blog.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = $this->validate($request,[
            'title'=>'required|max:100',
            'subtitle'=>'required|max:100',
            'content'=>'required'
        ]);

        $blog = new Blog();

        if (Input::hasFile('blogimg')) {
            $file = Input::file('blogimg');
            $file->move('upload',$file->getClientOriginalName());
            $blog->blogimg = $file->getClientOriginalName();
        }

        $blog->title = $request->title;
        $blog->subtitle = $request->subtitle;
        $blog->content = $request->content;
        // $blog->created_at = time();
        $blog->save();
        
        return redirect()->route('blog.show',$blog->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function show(Blog $blog)
    {
        //

        return view('blog.show',['blog'=>$blog]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function edit(Blog $blog)
    {
        //
        return view('blog.edit',['blog'=>$blog]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Blog $blog)
    {
        //
        $validate = $this->validate($request,[
            'title'=>'required|max:100',
            'subtitle'=>'required|max:100',
            'content'=>'required'
        ]);

        $model = Blog::find($blog->id);

        if (Input::hasFile('blogimg')) {
            $file = Input::file('blogimg');
            $file->move('upload',$file->getClientOriginalName());
            $model->blogimg = $file->getClientOriginalName();
        }

        $model->title = $request->title;
        $model->subtitle = $request->subtitle;
        $model->content = $request->content;
        $model->save();

        return redirect()->route('blog.show',$blog->id);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function destroy(Blog $blog)
    {
        $model = Blog::find($blog->id);
        $model->delete();
    }
}
