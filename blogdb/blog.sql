-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- 主機: 127.0.0.1:3306
-- 產生時間： 2018-04-24 07:15:16
-- 伺服器版本: 5.7.19
-- PHP 版本： 7.0.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- 資料庫： `blog`
--

-- --------------------------------------------------------

--
-- 資料表結構 `blogs`
--

DROP TABLE IF EXISTS `blogs`;
CREATE TABLE IF NOT EXISTS `blogs` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL COMMENT '標題',
  `subtitle` varchar(100) DEFAULT NULL COMMENT '副標題',
  `content` text NOT NULL COMMENT '內容',
  `blogimg` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=44 DEFAULT CHARSET=utf8;

--
-- 資料表的匯出資料 `blogs`
--

INSERT INTO `blogs` (`id`, `title`, `subtitle`, `content`, `blogimg`, `created_at`, `updated_at`) VALUES
(32, 'What is CRUD?', 'create read update destroy', 'Create: return the view of create blade\r\nRead: retrn the view of show blade\r\nUpdate: update the specific item\r\nDestroy: destroy the specific item\r\n\r\n# Controller\r\nindex() => return view(\'blog.index\')\r\nshow() => return view(\'blog.show\',$blog->id)\r\n\r\ncreate() => return view(\'blog.create\')\r\nstore() => store the result from blog.create\r\n\r\nedit() => return view(\'blog.edit\')\r\nupdate() => update the result from blog.edit\r\n\r\ndestroy() => delete specific item\r\n\r\n# Route\r\nblog.index	blog			GET\r\nblog.store	blog			POST\r\nblog.create	blog/{blog}		GET\r\nblog.show	blog/{blog}		GET\r\nblog.update	blog/{blog}		PATCH\r\nblog.destroy	blog/{blog}		DELETE\r\nblog.edit	blog/{blog}/edit	GET\r\n\r\n\r\n# PATCH,DELETE\r\n$.ajax({\r\n    url: \"{{ url(\'blog\') }}/\"+[id],\r\n    method: \"DELETE/PATCH\"\r\n    data: {\r\n        _token: \'{{ csrf_token() }}\'\r\n    }\r\n});\r\n\r\nOR\r\n\r\nform\r\n{{ csrf_field() }}\r\n{{ method_field(\"DELETE or PATCH\") }}', 'icon.png', '2018-04-20 22:12:45', '2018-04-23 23:02:26'),
(30, 'Deploy Laravel Project on unbuntu server', 'Some notes...', '#1 sudo apt-get update\r\n#2 sudo apt-get install apache2\r\n#3 install php\r\n#4 set document root\r\n#5 set permission of storage directory to 777\r\n sudo chmod -R 0777 storage/ bootstrap/cache/', 'ubuntu.ico', '2018-04-20 19:38:02', '2018-04-23 23:03:20'),
(33, 'Laravel Seeding', 'Way to create multiple random items easily', 'php artisan make:seed BlogsTableSeeder\r\n\r\nBlogsTableSeeder => run(){\r\n	DB:table(\'blogs\')->insert([...]);\r\n}\r\n\r\nDataBaseSeeder => run(){\r\n	$this->call(BlogsTableSeeder:calss);\r\n}', 'laravel-database-seeding.jpg', NULL, '2018-04-23 23:09:30'),
(34, 'Laravel Email', 'easy way to send gmail', '# /config/mail.php\r\n\'driver\' => env(\'MAIL_DRIVER\', \'smtp\'),\r\n\'host\' => env(\'MAIL_HOST\', \'smtp.gmail.com\'),\r\n\'port\' => env(\'MAIL_PORT\', 587),\r\n\'from\' => [\r\n	\'address\' => env(\'MAIL_FROM_ADDRESS\', \'hello@example.com\'),\r\n	 \'name\' => env(\'MAIL_FROM_NAME\', \'Example\'),\r\n],\r\n\'encryption\' => env(\'MAIL_ENCRYPTION\', \'tls\'),\r\n\r\n\'username\' => \'your@gmail.com\',\r\n\'password\' => \'yourpassword\',\r\n\r\n\r\n\r\n\r\n# .env\r\nMAIL_DRIVER=smtp\r\nMAIL_HOST=smtp.gmail.com\r\nMAIL_PORT=587\r\nMAIL_USERNAME=your@gmail.com\r\nMAIL_PASSWORD=yourpassword\r\nMAIL_ENCRYPTION=tls\r\n\r\n# Controller\r\nuse Mail;\r\n\r\n// 寄件資訊\r\n$from = [\r\n	\'email\'=>env(\"MAIL_USERNAME\",\"your@gmail.com\"),\r\n	\'name\'=>\"涵德(測試信件)\",\r\n	\'subject\'=>\"這是一封測試信\"\r\n];\r\n//填寫收信人信箱\r\n$to = [\r\n	\'email\'=>$request->email,\r\n	\'name\'=>$request->name\r\n];\r\n\r\n//信件的內容(即表單填寫的資料)\r\n$data = [\r\n	\'subject\'=>$from[\'subject\'],\r\n	\'msg\'=>\"你已經收到我的測試信~\",\r\n	\'comment\'=>$request->comment,\r\n	\'name\' => $request->name,\r\n];  \r\n\r\n//寄出信件\r\nMail::send(\'email.normal\', $data, function($message) use ($from, $to) {\r\n	$message->from($from[\'email\'], $from[\'name\']);\r\n	$message->to($to[\'email\'], $to[\'name\'])->subject($from[\'subject\']);\r\n});\r\n\r\n# 創建 email.normal\r\n\r\n# p.s.\r\n1. gmail 要開啟較低應用程式安全設定\r\n2. 在local開發測試階段有時候會因為ssl連線的安全機制被擋掉，需在\r\n/vendor/swiftmailer/swiftmailer/lib/classes/swift/Transport/StreamBuffer.php 的 _establishSocketConnection()\r\n的 $option 創建後馬上加入以下兩行\r\n$options[\'ssl\'][\'verify_peer\'] = FALSE;\r\n$options[\'ssl\'][\'verify_peer_name\'] = FALSE;\r\n\r\n不過正式上線的記得要拿掉', 'gmail_pre_md_1x.png', NULL, '2018-04-23 23:10:25'),
(41, 'HIgm8', 'AbMLpKF7pGF5gjy', '62SNt8lvZMe80OcpCY7vU99d2cdX3KEHAEgwIXkkf18c5cvW6x5ubaWfjGeQ3M0xmvFV3MhxD4dAxalXJJv4h1SwccIbatFWvONWphdo6p6q523tQjPUFHutOUsdnXn6xqPdslhAcGWO5MgRSGcl5yQRRBs08B6A9Z0t3wBBJUBXUR7rxnH3QWdY3h7r9CYPxuCx5AoQFFmDKtUkmLNOO0BUlFmuxpxE59BA4VyzWrN3TKg7KyDC7J0H58TGjogYFN0qVtXghagSETJl1Fej6f0T571AOEkxxYTXnP2zcoNFphRww3rOFUwVFXZHA5wP176loORD8mMe7s9dYq1VO63ufn4xwYdyXBlBwBW1c8nGYLBsTttIceQ5qz62U7o6jq8zC2Oc9buK5b5dxXc84Nnbg8MJSbUw3EhNfkyzq0Nm8wHV7oU37fnOxNf9yzQKc0aQFqxtVzaN2qVg3zNHzBCqs3uZjMplXZxkp63p5exaTxJD12FL', NULL, NULL, NULL),
(42, 'IkvHk', 'RiV7SRL9nMRxgKi', 'zdBrqbwOQ0fBg7rVOEesjQQZhSc2ZqsNyvBQkCYZ1CgKWVZwO98OQGb0EtuspXh12dN0wFbXgoRUb7ApmwL4SMsV4hitLeM1F5CrEXr6yyEZ2FsL8xZvjZQLbaCnDhFy51hejnd956d7VewpTAReHE7KxcjXOndPr9ikj7QWSS0QbDdMVrauxpmMTYC5fzEm1kDlbYbtd0b4eprcyM91LAkiu0GKfmfFQTZBwGzrA19eHALsAH5nqIpXznsr8AqwCpL4jfUVB9FqpKFCTNSFlRXpXSyILLA3CGvQJJlQ7oUHJotbVC9dxaKDalMboXV9Ir3luB7OVdcS4l2SRWa9LynG5vJipGFyaE4L6zpLnP98I2efP2nUtQa1arklI4rIzeZ4tKQIihJm8XcUWzXdgEDLePN8wJAhhMEiDPc6KjKmXbIRo1RhwI18iR5hyM9OgAbzLdemn2LTqV47Fy3ZutNrvQwFVoDVvrf00TaPFAkuMoWYXQvS', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- 資料表結構 `drinks`
--

DROP TABLE IF EXISTS `drinks`;
CREATE TABLE IF NOT EXISTS `drinks` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(10) NOT NULL,
  `mprice` int(11) NOT NULL,
  `lprice` int(11) NOT NULL,
  `is_hot` tinyint(1) NOT NULL,
  `sort` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- 資料表的匯出資料 `drinks`
--

INSERT INTO `drinks` (`id`, `name`, `mprice`, `lprice`, `is_hot`, `sort`, `created_at`, `updated_at`) VALUES
(1, '紅茶', 0, 25, 1, 0, NULL, NULL),
(2, '綠茶', 0, 25, 0, 0, NULL, NULL),
(3, '烏龍茶', 0, 25, 0, 0, NULL, NULL),
(4, '珍珠奶茶', 40, 50, 1, 0, NULL, NULL),
(5, '木瓜牛奶', 40, 50, 1, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- 資料表結構 `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- 資料表的匯出資料 `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_04_19_115948_create_drinks_table', 1),
(4, '2018_04_19_144618_create_blogs_table', 1);

-- --------------------------------------------------------

--
-- 資料表結構 `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
