<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Home
Route::get('/','HomeController@index')->name('home.index');
// Route::group(['prefix'=>'drink'],function(){
//     Route::get('/','DrinkController@index');
// });

// Vue
Route::prefix('drink')->group(function(){
    Route::get('/','DrinkController@index')->name('drink.index');
    Route::post('/get_drinks','DrinkController@get_drinks');
});

// CRUD
Route::resource('blog','BlogController');


// Email
Route::prefix('email')->group(function(){
    Route::get('/','MailController@index')->name('email.index');
    Route::post('/normal','MailController@normal')->name('email.normal');
    Route::post('/cart','MailController@cart')->name('email.cart');
    Route::get('/cart','MailController@cart')->name('email.cart2');
});

Route::get('sendmail', function() {
 $data = ['name' => 'Test'];
 Mail::send('email.welcome', $data, function($message) {
  $message->to('myginnygo@gmail.com')->subject('This is test email');
 });
 return 'Your email has been sent successfully!';
});